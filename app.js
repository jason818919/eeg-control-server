var HTTP_PORT = 80; // make sure the ports are allowed to be connected
var HTTPS_PORT = 443;
var TCP_PORT_EMOTIV = 54133;
var TCP_PORT_3DSMAX = 54134;
var TCP_PORT_RHINO = 54135;
var TCP_PORT_CUSTOM = 54136;
var TCP_PORT_PHOTOSHOP = 54137;
var TCP_PORT_ARDUINO = 54138;
var logger = require('./libs/logger'); // log all the request from the clients
var ssl = require('./libs/sslLicense'); // source the ssl keys
var url = require('url');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var https = require('https').Server(ssl.options, app);
var io = require("socket.io")(https);
var net = require("net");
var EventEmitter = require('events').EventEmitter;
var threedsmax = new EventEmitter();
var rhino = new EventEmitter();
var photoshop = new EventEmitter();
var arduino = new EventEmitter();
var custom = new EventEmitter();
app.use(logger);

var current_client_app = "Rhino";
var current_filter = false;
app.use(function(request, response, next){ // redirect the http to https
	if(!request.secure){
		var urlObj = url.parse('http://' + request.headers.host + request.url);
		var returnUrl = 'https://' + urlObj.hostname + ':' + HTTPS_PORT + urlObj.pathname;	
		console.log('[HTTP TO HTTPS] unsecure request, redirect to ' + returnUrl);
		return response.redirect(302, returnUrl);
	}
	next();
});

app.use(express.static(__dirname+'/public')); // allow everyone the gets the resource in public folder

io.on('connection', function (socket) { // use socket.io as the communication bridge between client and server
	// 0 neutral, 1 zoom in, 2 zoom out, 3 rotate left, 4 rotate right
	socket.on('my other event', function (data) {
		console.log('[SOCKET.IO] ' + data);
	});
	socket.on("toggle", function(obj){
		if(current_client_app=="Rhino"){
			current_client_app = "3ds Max";
		}else if(current_client_app=="3ds Max"){
			current_client_app = "Photoshop";
		}else if(current_client_app=="Photoshop"){
			current_client_app = "Arduino";
		}else if(current_client_app=="Arduino"){
			current_client_app = "Custom Usage";
		}else{
			current_client_app = "Rhino";
		}
		io.emit("app_status", current_client_app);
		console.log('[SOCKET.IO STATUS] current_client_app: ' + current_client_app);
	});

	socket.on("filter", function(obj){
		current_filter = ! current_filter;
		if(current_filter){
			io.emit("filter_status", "ON");
		}else{
			io.emit("filter_status", "OFF");
		}
	});
	//sendPacket(1);
});

http.listen(HTTP_PORT, function(){ // http listen on a specified port
	console.log('[HTTP] server start listening on port ' + HTTP_PORT + ' ...');
});

https.listen(HTTPS_PORT, function(){ // https listen on a specified port
	console.log('[HTTPS] server start listening on port ' + HTTPS_PORT + ' ...');
});

var server_emotiv = net.createServer(function(client){
	var buffer=[];
	console.log('[NET SERVER EMOTIV] receive connection from ' + client.remoteAddress + ':' + client.remotePort);
	client.on("error", function(error){
		console.log("[NET SERVER EMOTIV] error message: " + error);
		client.end();
	});

	client.on("end",function(){
		console.log("[NET SERVER EMOTIV] end connection from: " + client.remoteAddress + ":" + client.remotePort);
	});

	client.on("data", function(data){
		//console.log("[NET SERVER] receive data: " + data);
		var data=data.toString().match(/[^{}]+(?=\})/g);
		//console.log(data);
		for(var i=0;i<data.length;++i){
			data[i]="{"+data[i]+"}";
			if(isJson(data[i])){
				var data_obj = JSON.parse(data[i]);
				var obj = {};
				obj.id = data_obj.id;
				obj.value = data_obj.value;
				//console.log("[NET SERVER] transform to obj: " + JSON.stringify(obj));
				if(current_filter){
					if(Number(obj.value)<30){
						obj.id="0";
					}
				}
				io.emit("news", obj);
				if(current_client_app=="Rhino"){
					rhino.emit("send", obj.id);
				}else if(current_client_app=="3ds Max"){
					threedsmax.emit("send", obj.id);
				}else if(current_client_app=="Photoshop"){
					photoshop.emit("send", obj.id);
				}else if(current_client_app=="Arduino"){
					arduino.emit("send", obj.id);
				}else{
					custom.emit("send", JSON.stringify(obj));
				}
			}	
		}
	});
	client.write("emotiv");
});
var server_threedsmax = net.createServer(function(client){
	console.log('[NET SERVER threedsmax] receive connection from ' + client.remoteAddress + ':' + client.remotePort);
	client.on("error", function(error){
		//console.log("[NET SERVER threedsmax] error message: " + error);
		client.end();
	});

	client.on("end",function(){
		console.log("[NET SERVER threedsmax] end connection from: " + client.remoteAddress + ":" + client.remotePort);
	});

	client.on("data", function(data){
		console.log("[NET SERVER threedsmax] receive data: " + data);
	});
	client.write("threedsmax");
	threedsmax.on('send', function(str){
		client.write(str);
	});
});
var server_rhino = net.createServer(function(client){
	console.log('[NET SERVER Rhino] receive connection from ' + client.remoteAddress + ':' + client.remotePort);
	client.on("error", function(error){
		//console.log("[NET SERVER Rhino] error message: " + error);
		client.end();
	});

	client.on("end",function(){
		console.log("[NET SERVER Rhino] end connection from: " + client.remoteAddress + ":" + client.remotePort);
	});

	client.on("data", function(data){
		console.log("[NET SERVER Rhino] receive data: " + data);
	});
	client.write("rhino");
	rhino.on('send', function(str) {
		client.write(str);
	});
});
var server_photoshop = net.createServer(function(client){
	console.log('[NET SERVER Photoshop] receive connection from ' + client.remoteAddress + ':' + client.remotePort);
	client.on("error", function(error){
		//console.log("[NET SERVER Photoshop] error message: " + error);
		client.end();
	});

	client.on("end",function(){
		console.log("[NET SERVER Photoshop] end connection from: " + client.remoteAddress + ":" + client.remotePort);
	});

	client.on("data", function(data){
		console.log("[NET SERVER Photoshop] receive data: " + data);
	});
	client.write("photoshop");
	photoshop.on('send', function(str) {
		client.write(str);
	});
});
var server_arduino = net.createServer(function(client){
	console.log('[NET SERVER Arduino] receive connection from ' + client.remoteAddress + ':' + client.remotePort);
	client.on("error", function(error){
		//console.log("[NET SERVER Arduino] error message: " + error);
		client.end();
	});

	client.on("end",function(){
		console.log("[NET SERVER Arduino] end connection from: " + client.remoteAddress + ":" + client.remotePort);
	});

	client.on("data", function(data){
		console.log("[NET SERVER Arduino] receive data: " + data);
	});
	client.write("arduino");
	arduino.on('send', function(str) {
		client.write(str);
	});
});
var server_custom = net.createServer(function(client){
	console.log('[NET SERVER Custom] receive connection from ' + client.remoteAddress + ':' + client.remotePort);
	client.on("error", function(error){
		//console.log("[NET SERVER Custom] error message: " + error);
		client.end();
	});

	client.on("end",function(){
		console.log("[NET SERVER Custom] end connection from: " + client.remoteAddress + ":" + client.remotePort);
	});

	client.on("data", function(data){
		console.log("[NET SERVER Custom] receive data: " + data);
	});
	client.write("custom");
	custom.on('send', function(str) {
		client.write(str);
	});
});

server_emotiv.listen(TCP_PORT_EMOTIV, function(){
	console.log("[NET SERVER EMOTIV] start listening on port " + TCP_PORT_EMOTIV + "...");
});
server_threedsmax.listen(TCP_PORT_3DSMAX, function(){
	console.log("[NET SERVER 3ds max] start listening on port " + TCP_PORT_3DSMAX	 + "...");
});
server_rhino.listen(TCP_PORT_RHINO, function(){
	console.log("[NET SERVER Rhino] start listening on port " + TCP_PORT_RHINO + "...");
});
server_photoshop.listen(TCP_PORT_PHOTOSHOP, function(){
	console.log("[NET SERVER Photoshop] start listening on port " + TCP_PORT_PHOTOSHOP + "...");
});
server_arduino.listen(TCP_PORT_ARDUINO, function(){
	console.log("[NET SERVER Arduino] start listening on port " + TCP_PORT_ARDUINO + "...");
});
server_custom.listen(TCP_PORT_CUSTOM, function(){
	console.log("[NET SERVER Custom] start listening on port " + TCP_PORT_CUSTOM + "...");
});

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
