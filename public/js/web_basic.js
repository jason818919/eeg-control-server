$(document).ready(function(){
	console.log("web_basic.js is executing...");

	var totalPoints = 500;
	var shadowPoints = 20;
// setup plots
	var data1=[];
	for(var i=0;i<totalPoints;i++){
		data1.push([i,null]);
	}

	var data2=[];
	for(var i=0;i<totalPoints;i++){
		data2.push([i,null]);
	}
	var data3=[];
	for(var i=0;i<totalPoints;i++){
		data3.push([i,null]);
	}
	var data4=[];
	for(var i=0;i<totalPoints;i++){
		data4.push([i,null]);
	}

	var options = function(x,ymin,ylen){
		h={};	
		h.series={shadowSize:0, color: x,lines: {lineWidth: 4 } };
		h.yaxis={show:true, min:ymin,max:ylen-1, font:{size:"24px"}};
		h.xaxis={show:true, font:{size:"24px"}};
		h.grid={show: true, aboveData: false, autoHighlight: true, hoverable: false, clickable:false, borderWidth: 1, borderColor: "#fff", color: "#555"};
		h.legend={backgroundOpacity: 0, aboveData: true, position:"nw", show: true};
		return h;
	};
	
	var waveform1=[];
	var t1=0;
	var tmp1;
	var monitor1 = $.plot($("#zoom_in"), [{label: "Zoom in", data: data1}], options("red",0,100));

	var waveform2=[];
	var t2=0;
	var tmp2;
	var monitor2 = $.plot($("#zoom_out"), [{label: "Zoom out", data: data2}], options("black",0,100));

	var waveform3=[];
	var t3=0;
	var tmp3;
	var monitor3 = $.plot($("#rotate_left"), [{label: "Rotate left", data: data3}], options("green",0,100));
	
	var waveform4=[];
	var t4=0;
	var tmp4;
	var monitor4 = $.plot($("#rotate_right"), [{label: "Rotate right", data: data4}], options("blue",0,100));

	var socket = io.connect("/");
	socket.on("connect", function(){
		console.log("socket.io connection established...");
		drawPlot();
	});
	socket.on("disconnect", function(){
		console.log("socket.io disconnection");
	});
	socket.on("news", function(obj){
		if(obj.id==0){
			$(".current_command").text(" neutral ");
			$(".current_command").css("background-color", "orange");
			waveform1=[0];
			waveform2=[0];
			waveform3=[0];
			waveform4=[0];
			updateBuffer();
		}
		if(obj.id==1){
			$(".current_command").text(" zoom in ");
			$(".current_command").css("background-color", "red");
			waveform1=[obj.value];
			waveform2=[0];
			waveform3=[0];
			waveform4=[0];
			updateBuffer();
		}
		if(obj.id==2){//zoom in for plot 1
			$(".current_command").text(" zoom out ");
			$(".current_command").css("background-color", "black");
			waveform2=[obj.value];
			waveform1=[0];
			waveform3=[0];
			waveform4=[0];
			updateBuffer();
		}
		if(obj.id==3){
			$(".current_command").text(" rotate left ");
			$(".current_command").css("background-color", "green");
			waveform3=[obj.value];
			waveform1=[0];
			waveform2=[0];
			waveform4=[0];
			updateBuffer();
		}
		if(obj.id==4){//zoom in for plot 1
			$(".current_command").text(" rotate right ");
			$(".current_command").css("background-color", "blue");
			waveform4=[obj.value];
			waveform1=[0];
			waveform2=[0];
			waveform3=[0];
			updateBuffer();
		}
	});
	socket.emit("my other event", "client hello!");
	
	function updateBuffer(){
		for(var i=0;i<waveform1.length;i++){
			t1=(t1+1)%totalPoints;
			tmp1=(t1+shadowPoints)%totalPoints;
			data1[t1]=[t1,waveform1[i]];
			data1[tmp1]=[tmp1,null];
		}
		for(var i=0;i<waveform2.length;i++){
			t2=(t2+1)%totalPoints;
			tmp2=(t2+shadowPoints)%totalPoints;
			data2[t2]=[t2,waveform2[i]];
			data2[tmp2]=[tmp2,null];
		}
		for(var i=0;i<waveform3.length;i++){
			t3=(t3+1)%totalPoints;
			tmp3=(t3+shadowPoints)%totalPoints;
			data3[t3]=[t3,waveform3[i]];
			data3[tmp3]=[tmp3,null];
		}
		for(var i=0;i<waveform4.length;i++){
			t4=(t4+1)%totalPoints;
			tmp4=(t4+shadowPoints)%totalPoints;
			data4[t4]=[t4,waveform4[i]];
			data4[tmp4]=[tmp4,null];
		}

		waveform1=[];
		waveform2=[];
		waveform3=[];
		waveform4=[];
	}

	
	function drawPlot(){
		setTimeout(function(){
			monitor1.resize();
			monitor1.setupGrid();
			monitor1.setData([{label: "Zoom in", data: data1}]);
			monitor1.draw();
			monitor2.resize();
			monitor2.setupGrid();
			monitor2.setData([{label: "Zoom out", data: data2}]);
			monitor2.draw();
			monitor3.resize();
			monitor3.setupGrid();
			monitor3.setData([{label: "Rotate left", data: data3}]);
			monitor3.draw();
			monitor4.resize();
			monitor4.setupGrid();
			monitor4.setData([{label: "Rotate right", data: data4}]);
			monitor4.draw();
			drawPlot();
		},30);
	}
	$("#toggle").on("click", function(e){
		e.preventDefault();
		console.log("toggle click");
		socket.emit("toggle","hello");
	});
	$("#filter").on("click", function(e){
		e.preventDefault();
		console.log("filter click");
		socket.emit("filter","hello");
	});
	socket.on("app_status", function(active){
		console.log("receive app status: " + active);
		$(".status_active").text(active);
	});
	socket.on("filter_status", function(active){
		console.log("receive filter status: " + active);
		$(".current_filter").text(active);
		if(active=="ON"){
			$("#filter").text("Disable the filter");
			$(".current_filter").css("color","green");
		}else{
			$("#filter").text("Enable the filter");
			$(".current_filter").css("color","red");
		}
	});
	socket.emit("filter","hello");
	socket.emit("toggle","hello");
});
