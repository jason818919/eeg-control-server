# Synopsis

This is the server part of the EEG control project which is responsible for handling the request from EMOTIV headsets, forwarding to client applications in real-time
and establishing a user control space on the web.

The back-end is built on [Node.js](https://nodejs.org/en/), [Express](http://expressjs.com) and [socket.io](http://socket.io), on the other hand, front-end is
[Flot](http://www.flotcharts.org), [Bootstrap](http://getbootstrap.com) and [jQuery](https://jquery.com).

# Motivation

To create a lot of interesting Internet of Things(IoT) applications everywhere, we need to get all mental commands from EMOTIV headsets on the internet. When the EEG
control server is established, everyone can fetch the mental commands easily in real-time from the EMOTIV headsets if one is on the internet. This client-server based
mechanism extends the user span from the one computer that is installed with EMOTIV engine to all platforms if it is on internet. Moreover, the EEG control server
provides a user control space on the web to visualize the mental command status and a filtering option is included.

# Installation

Download the source code and run the installation script as follows.
```bash
wget https://bitbucket.org/jason818919/eeg-control-server/get/master.tar.gz
tar -xvz -f master.tar.gz
rm master.tar.gz
mv *eeg-control-server* eeg-control-server
cd eeg-control-server
./install
```

# Tests

Run the application on terminal.
```bash
node app.js
```
# Contributor

Jason Chen